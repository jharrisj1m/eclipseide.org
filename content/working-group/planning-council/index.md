---
title: "Planning Council"
date: 2022-10-13T11:33:28-04:00
categories: []
keywords:
    - foundation
    - legal
    - councils
    - emo
slug: ""
aliases: []
toc: false
hide_sidebar: true
container: container padding-bottom-20
---

The Planning Council is responsible for establishing a coordinated Platform Release Plan that supports the Roadmap, and balances the many competing requirements. The Platform Release Plan describes the themes and priorities that focus these Releases, and orchestrates the dependencies among Project Plans.

{{< pages/working-group/planning-council/council_table class="padding-top-20" council="planning" >}}

- [Planning Council's wiki page and meeting minutes](https://github.com/eclipse-simrel/.github/blob/main/wiki/Planning_Council.md).

*\* chair*